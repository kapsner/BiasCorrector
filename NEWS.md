# BiasCorrector NEWS

## Unreleased (2022-06-20)

#### Others

* added dependencies badge
* updated lintr
* updated news.md
* updated news.md; updated reamde badges
* removed context from unittests

Full set of changes: [`v0.2.1...e2affe4`](https://github.com/kapsner/BiasCorrector/compare/v0.2.1...e2affe4)

## v0.2.1 (2022-02-16)


Full set of changes: [`v0.2.0...v0.2.1`](https://github.com/kapsner/BiasCorrector/compare/v0.2.0...v0.2.1)

## v0.2.0 (2021-05-17)


Full set of changes: [`v0.1.4...v0.2.0`](https://github.com/kapsner/BiasCorrector/compare/v0.1.4...v0.2.0)

## v0.1.4 (2021-03-29)


Full set of changes: [`v0.1.3...v0.1.4`](https://github.com/kapsner/BiasCorrector/compare/v0.1.3...v0.1.4)

## v0.1.3 (2021-03-01)


Full set of changes: [`v0.1.2...v0.1.3`](https://github.com/kapsner/BiasCorrector/compare/v0.1.2...v0.1.3)

## v0.1.2 (2020-09-22)


Full set of changes: [`v0.1.1...v0.1.2`](https://github.com/kapsner/BiasCorrector/compare/v0.1.1...v0.1.2)

## v0.1.1 (2020-08-16)


Full set of changes: [`v0.1.0...v0.1.1`](https://github.com/kapsner/BiasCorrector/compare/v0.1.0...v0.1.1)

## v0.1.0 (2020-07-18)


Full set of changes: [`v0.0.7...v0.1.0`](https://github.com/kapsner/BiasCorrector/compare/v0.0.7...v0.1.0)

## v0.0.7 (2020-06-18)


Full set of changes: [`v0.0.6...v0.0.7`](https://github.com/kapsner/BiasCorrector/compare/v0.0.6...v0.0.7)

## v0.0.6 (2020-01-18)


Full set of changes: [`v0.0.5...v0.0.6`](https://github.com/kapsner/BiasCorrector/compare/v0.0.5...v0.0.6)

## v0.0.5 (2019-12-16)


Full set of changes: [`v0.0.4...v0.0.5`](https://github.com/kapsner/BiasCorrector/compare/v0.0.4...v0.0.5)

## v0.0.4 (2019-12-09)


Full set of changes: [`v0.0.3...v0.0.4`](https://github.com/kapsner/BiasCorrector/compare/v0.0.3...v0.0.4)

## v0.0.3 (2019-11-16)


Full set of changes: [`v0.0.2...v0.0.3`](https://github.com/kapsner/BiasCorrector/compare/v0.0.2...v0.0.3)

## v0.0.2 (2019-11-09)


Full set of changes: [`v0.0.899...v0.0.2`](https://github.com/kapsner/BiasCorrector/compare/v0.0.899...v0.0.2)

## v0.0.899 (2019-06-20)


Full set of changes: [`v0.9.4...v0.0.899`](https://github.com/kapsner/BiasCorrector/compare/v0.9.4...v0.0.899)

## v0.9.4 (2019-04-24)


Full set of changes: [`v0.9.3...v0.9.4`](https://github.com/kapsner/BiasCorrector/compare/v0.9.3...v0.9.4)

## v0.9.3 (2019-04-18)


Full set of changes: [`v0.9.2...v0.9.3`](https://github.com/kapsner/BiasCorrector/compare/v0.9.2...v0.9.3)

## v0.9.2 (2019-04-18)


Full set of changes: [`v0.9.1...v0.9.2`](https://github.com/kapsner/BiasCorrector/compare/v0.9.1...v0.9.2)

## v0.9.1 (2019-03-02)


Full set of changes: [`v0.9.0...v0.9.1`](https://github.com/kapsner/BiasCorrector/compare/v0.9.0...v0.9.1)

## v0.9.0 (2019-01-26)

